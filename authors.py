import pandas as pd

from classifiers.deep_learning.cnn import EmbeddedSmallCNNClassifier
from classifiers.old_guard.svc import SVCTFID

path = "./data/authors"


def load_data():
    train = pd.read_csv(path + '/train.csv')
    classes = ['EAP', 'HPL', 'MWS']
    return zip(train.text.values, train.author.values), classes


# Best Score: 0.44
if __name__ == '__main__':
    traning_data, classes = load_data()
    model = EmbeddedSmallCNNClassifier(traning_data, classes, 'authors')
    model.train()
