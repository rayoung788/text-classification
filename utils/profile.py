from time import time

import functools


def profile(f):
    @functools.wraps(f)
    def timer(*args, **kwargs):
        start = time()
        result = f(*args, **kwargs)
        f_time = "{}: {} seconds".format(f.__name__, round(time() - start, 5))
        print(f_time)
        return result
    return timer
