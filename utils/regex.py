import re
allowed_currencies = """¥£₪$€฿₨"""
allowed_punctuation = """-!?/;"'%&<>.()[]{}@#:,|=*"""
sent_regex = r"""(?<!\w['"])(?<!,['"])(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?\n!"'])\s"""
word_regex = r"\b[^\s()\"<>,:;{}\[\].]+\b"
clean_regex = [
            # whitespace norm
            (re.compile(r'[^\S\n]+', re.U), ' '),
            # dashes
            (re.compile(r'[\-˗֊‐‑‒–—⁻₋−﹣－]', re.U), '-'),
            # apostrophes
            (re.compile(r'&#39;|[ʼ՚＇‘’‛❛❜ߴߵ`‵´ˊˋ{}{}{}{}{}{}{}{}{}]'.format(chr(768), chr(769), chr(832), chr(833),
                                                                            chr(2387), chr(5151), chr(5152), chr(65344),
                                                                            chr(8242)), re.U), '\''),
            # double quotes
            (re.compile(r'[“”]'), '"'),
            # left paren
            (re.compile(r'[(\[{⁽₍❨❪﹙（]', re.U), '('),
            # right paren
            (re.compile(r'[)\]\}⁾₎❩❫﹚）]', re.U), ')'),
            # remove (one-word) chapter titles
            (re.compile(r'^[\w]+\s*$\s*', re.M), ''),
            # fix sentence breaks
            (re.compile(r'(?<!(\. ))\n+ \n*(?=([\w]|"[a-z]))', re.U), ''),
            # new-line norm
            (re.compile(r'\n\s*', re.U), '\n\n'),
            # misc
            (re.compile(r'[^\w\s{}{}]'.format(re.escape(allowed_currencies), re.escape(allowed_punctuation)),
                        re.U), '')
        ]
