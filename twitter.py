import pandas as pd
from pickle import dump, load

from os.path import exists
from tqdm import tqdm

from classifiers.old_guard.benchmark import Benchmark
from utils.profile import profile


path = "./data/twitter"
twitter_path = path + "/twitter.p"
# http://thinknook.com/twitter-sentiment-analysis-training-corpus-dataset-2012-09-22/


@profile
def import_twitter():
    df = pd.read_csv(path + '/twitter.csv', error_bad_lines=False)
    labels, docs = list(), list()
    for i, row in tqdm(df.iterrows()):
        labels.append(row['Sentiment'])
        docs.append(row['SentimentText'])
    data = zip(docs, labels)
    classes = ["negative", "positive"]
    dump((data, classes), open(twitter_path, "wb"))
    return data, classes


@profile
def collect():
    return load(open(twitter_path, "rb")) if exists(twitter_path) else import_twitter()


def old_guard():
    data, classes = collect()
    cls = Benchmark(data, classes, "twitter")
    cls.benchmark()


def cnn():
    from classifiers.deep_learning.cnn import SmallCNNClassifier
    data, classes = collect()
    return SmallCNNClassifier(data, classes, "twitter")


if __name__ == "__main__":
    print(cnn().predict(["thats good", "thats bad"]))
