from pickle import load

from classifiers.deep_learning.cnn import SmallCNNClassifier, EmbeddedSmallCNNClassifier, CNNClassifier
from classifiers.deep_learning.fasttext import Fasttext
from classifiers.old_guard.svc import SVCTFID

categories = {
        'anthropology': 5100,
        'astronomy': 9540,
        'biology': 6000,
        'business': 5200,
        'chemistry': 9110,
        'communication': 1000,
        'computer_science': 9520,
        # 'earth_science',
        'economics': 5300,
        'education': 2000,
        'engineering': 9530,
        'geography': 5400,
        'geology': 7000,
        'medicine': 8000,
        'history': 5500,
        'language': 3000,
        'linguistics': 3000,
        'literature': 3000,
        'law': 5600,
        'mathematics': 9120,
        'philosophy': 4000,
        'physics': 9130,
        # 'physical_sciences',
        'political_science': 5700,
        'psychology': 10000,
        # 'social_science',
        # 'social_work',
        'sociology': 5800,
        'transportation': 5950,
        'women_studies': 5960
    }

data_path = "./data/academic/clean/"


def load_data():
    results = []
    classes = categories.keys()
    for i, category in enumerate(classes):
        data = list(load(open(data_path + category + ".p", "rb")).values())
        results.extend(zip(data, [i] * len(data)))
    return results, list(classes)


# 0.861
if __name__ == '__main__':
    res, cls = load_data()
    model = CNNClassifier(res, cls, 'academic')
    model.train()
