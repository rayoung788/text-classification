
from keras.layers import Conv1D, Embedding, Dense, MaxPooling1D, Flatten, SpatialDropout1D, Dropout, GlobalMaxPooling1D
from keras.models import Sequential

from classifiers.deep_learning.embedding import EmbeddingNN
from classifiers.deep_learning.nn import NNClassifier


class CNNClassifier(NNClassifier):
    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, 50, input_length=self.max_len),
            SpatialDropout1D(0.2),
            Dropout(0.2),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.2),
            MaxPooling1D(5),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.2),
            MaxPooling1D(5),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.2),
            GlobalMaxPooling1D(),
            Dense(250, activation='relu'),
            Dropout(0.2),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        return model


class SmallCNNClassifier(NNClassifier):
    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, 50, input_length=self.max_len),
            Dropout(0.2),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            GlobalMaxPooling1D(),
            Dense(250, activation='relu'),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        return model


class EmbeddedSmallCNNClassifier(EmbeddingNN):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir, "glove.840B.300")
        self.epochs = 100

    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, self.em_dim, weights=[self.embedding_matrix],
                      input_length=self.max_len, trainable=False),
            SpatialDropout1D(0.3),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.3),
            GlobalMaxPooling1D(),
            Dense(250, activation='relu'),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        return model


class EmbeddedCNNClassifier(EmbeddingNN):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir, "glove.6B.50")
        self.epochs = 12

    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, self.em_dim, weights=[self.embedding_matrix],
                      input_length=self.max_len, trainable=False),
            Dropout(0.2),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.2),
            MaxPooling1D(5),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.2),
            MaxPooling1D(5),
            Conv1D(250, 3, activation='relu', strides=1, padding='valid'),
            Dropout(0.2),
            GlobalMaxPooling1D(),
            Dense(250, activation='relu'),
            Dropout(0.2),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        return model
