from keras.layers import Embedding, SpatialDropout1D, LSTM, Dense, Dropout, Bidirectional, GRU
from keras.models import Sequential

from classifiers.deep_learning.embedding import EmbeddingNN


class EmbeddedLSTMClassifier(EmbeddingNN):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir, "glove.840B.300")
        self.max_len = 70
        self.batch_size = 512
        self.epochs = 100

    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, self.em_dim, weights=[self.embedding_matrix],
                      input_length=self.max_len, trainable=False),
            SpatialDropout1D(0.3),
            LSTM(100, dropout=0.3, recurrent_dropout=0.3),
            Dense(1024, activation='relu'),
            Dropout(0.8),
            Dense(1024, activation='relu'),
            Dropout(0.8),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model


class EmbeddedBiDirClassifier(EmbeddingNN):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir, "glove.840B.300")
        self.max_len = 70
        self.batch_size = 512
        self.epochs = 100

    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, self.em_dim, weights=[self.embedding_matrix],
                      input_length=self.max_len, trainable=False),
            SpatialDropout1D(0.3),
            Bidirectional(LSTM(300, dropout=0.3, recurrent_dropout=0.3)),
            Dense(1024, activation='relu'),
            Dropout(0.8),
            Dense(1024, activation='relu'),
            Dropout(0.8),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model


class EmbeddedGRUClassifier(EmbeddingNN):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir, "glove.840B.300")
        self.max_len = 70
        self.batch_size = 512
        self.epochs = 100

    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, self.em_dim, weights=[self.embedding_matrix],
                      input_length=self.max_len, trainable=False),
            SpatialDropout1D(0.3),
            GRU(300, dropout=0.3, recurrent_dropout=0.3, return_sequences=True),
            GRU(300, dropout=0.3, recurrent_dropout=0.3),
            Dense(1024, activation='relu'),
            Dropout(0.8),
            Dense(1024, activation='relu'),
            Dropout(0.8),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        return model
