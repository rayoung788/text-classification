import codecs
from os.path import exists

import mmap
from tqdm import tqdm

from classifiers.deep_learning.nn import NNClassifier
from pickle import load, dump
import numpy as np

from utils.profile import profile


class EmbeddingNN(NNClassifier):
    def __init__(self, training_data, classes, sub_dir, embeddings):
        super().__init__(training_data, classes, sub_dir)
        self.embeddings = embeddings
        self.em_dim = int(embeddings.split('.')[-1]) if embeddings else 0
        self.embedding_matrix = np.zeros((1, self.em_dim))
        self.em_path = "./embeddings"

    def build(self):
        self.transform()
        self.build_em_matrix()
        self.save()

    @property
    def model_path(self):
        return "{}/models/{}/{}_{}.p".format(self.cache, self.sub_dir, self.__class__.__name__, self.embeddings)

    @property
    def weights_path(self):
        return "{}/weights/{}/{}_{}_weights.hdf5".format(self.cache, self.sub_dir, self.__class__.__name__,
                                                         self.embeddings)

    @property
    def pickled_em_path(self):
        return "{}/parsed/{}.p".format(self.em_path, self.embeddings)

    @property
    def raw_em_path(self):
        return "{}/raw/{}d.txt".format(self.em_path, self.embeddings)

    @profile
    def build_em_matrix(self):
        if self.embeddings:
            embeddings_index = load(open(self.pickled_em_path, "rb")) if exists(self.pickled_em_path)\
                else self.load_embeddings()
            self.embedding_matrix = np.zeros((len(self.vocab) + 1, self.em_dim))
            for word, i in self.vocab.items():
                embedding_vector = embeddings_index.get(word)
                self.embedding_matrix[i] = np.random.normal(scale=0.6, size=(self.em_dim,)) \
                    if embedding_vector is None else embedding_vector

    @profile
    def load_embeddings(self):
        embeddings_index = {}
        path = self.raw_em_path
        with codecs.open(path, 'r', "utf-8") as f:
            for i, line in tqdm(enumerate(f), desc="building embedding matrix", total=self.get_line_number(path)):
                values = line.split()
                try:
                    word = values[0]
                    coefs = np.asarray(values[1:], dtype='float32')
                    embeddings_index[word] = coefs
                except (ValueError, IndexError):
                    continue
        dump(embeddings_index, open(self.pickled_em_path, "wb"))
        return embeddings_index

    @staticmethod
    def get_line_number(file_path):
        fp = open(file_path, "r+")
        buf = mmap.mmap(fp.fileno(), 0)
        lines = 0
        while buf.readline():
            lines += 1
        return lines
