from keras.layers import Embedding, GlobalMaxPooling1D, Dense
from keras.models import Sequential

from classifiers.deep_learning.nn import NNClassifier


class Fasttext(NNClassifier):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self.max_features = 20000

    def build_model(self):
        model = Sequential([
            Embedding(len(self.vocab) + 1, 50, input_length=self.max_len),
            GlobalMaxPooling1D(),
            Dense(len(self.classes), activation='softmax'),
        ])
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        return model
