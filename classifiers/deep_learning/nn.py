import numpy as np
from collections import defaultdict

import re

from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.preprocessing import sequence
from keras.utils import to_categorical
from os.path import exists

from tqdm import tqdm

from classifiers.classifier import Classifier
from utils.profile import profile
from utils.regex import word_regex
# import spacy
#
# nlp = spacy.load('en')


class NNClassifier(Classifier):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self.vocab = dict()
        self.max_features = 5000
        self.max_len = 100
        self.batch_size = 32
        self.epochs = 30

    @property
    def weights_path(self):
        return "./cache/weights/{}/{}_weights.hdf5".format(self.sub_dir, self.__class__.__name__)

    @profile
    def post_init(self):
        self.load() if exists(self.model_path) else self.build()
        self._model = self.build_model()
        if exists(self.weights_path):
            print("loaded weights")
            self._model.load_weights(self.weights_path)

    def build(self):
        self.transform()
        self.save()

    @profile
    def transform(self):
        self.vocab = dict()
        words = defaultdict(int)
        docs = []
        for text in tqdm(self.x):
            doc = [word for word in re.findall(word_regex, text)]
            for word in doc:
                words[word] += 1
            docs.append(doc)
        sorted_words = [(word, cnt) for cnt, word in sorted(zip(words.values(), words.keys()), reverse=True)]
        self.vocab = {word: (i if i < self.max_features else self.max_features)
                      for i, (word, cnt) in enumerate(sorted_words)}
        self.x = [[self.vocab[x] for x in doc] for doc in docs]
        self.x = sequence.pad_sequences(self.x, maxlen=self.max_len)
        self.y = to_categorical(np.array(self.y), len(self.classes))
        self.split()

    @profile
    def build_model(self):
        pass

    @profile
    def train(self):
        model = self._model
        check_pointer = ModelCheckpoint(filepath=self.weights_path, monitor='val_acc',
                                        verbose=1, save_best_only=True)
        early_stopping = EarlyStopping(monitor='val_acc', verbose=1, patience=3)
        model.fit(
            self.x_train, self.y_train, batch_size=self.batch_size, epochs=self.epochs,
            validation_data=[self.x_test, self.y_test], callbacks=[check_pointer, early_stopping], verbose=1
        )
        # model.save(filepath='{}model.h5'.format(self.cached_data))

    @profile
    def predict(self, data):
        docs = [[word.lower() for word in re.findall(word_regex, doc)] for doc in data]
        x = sequence.pad_sequences(
            [[self.vocab[x] if x in self.vocab else self.max_features for x in doc] for doc in docs],
            maxlen=self.max_len
        )
        return self._model.predict(x)
