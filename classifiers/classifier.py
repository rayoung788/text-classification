from os.path import exists

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from pickle import load, dump
import numpy as np

from utils.meta import PostInit
from utils.profile import profile


class Classifier(metaclass=PostInit):
    def __init__(self, training_data, classes, sub_dir):
        self.x, self.y = zip(*training_data)
        self.x_train, self.x_test, self.y_train, self.y_test = [], [], [], []
        self.label_encoder = preprocessing.LabelEncoder()
        self.y = self.label_encoder.fit_transform(self.y)
        self.classes = classes
        self._model = None
        self.valid_split = 0.2
        self.sub_dir = sub_dir
        self.cache = "./cache"

    @profile
    def post_init(self):
        self.load() if exists(self.model_path) else self.build()

    def split(self):
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            self.x, self.y, test_size=self.valid_split, random_state=42)

    @property
    def model_path(self):
        return "{}/models/{}/{}.p".format(self.cache, self.sub_dir, self.__class__.__name__)

    def build(self):
        self.transform()
        self.save()

    @profile
    def transform(self):
        pass

    @profile
    def train(self):
        self._model.fit(self.x_train, self.y_train)

    @profile
    def predict(self, data):
        return self._model.predict(data)

    def cache_data(self):
        dump(self, open(self.model_path, "wb"))

    @profile
    def save(self):
        dump(self, open(self.model_path, "wb"))

    @profile
    def load(self):
        cls = load(open(self.model_path, "rb"))
        self.__dict__ = cls.__dict__

    @staticmethod
    def multiclass_logloss(actual, predicted, eps=1e-15):
        """Multi class version of Logarithmic Loss metric.
        :param actual: Array containing the actual target classes
        :param predicted: Matrix with class predictions, one probability per class
        """
        # Convert 'actual' to a binary array if it's not already:
        if len(actual.shape) == 1:
            actual2 = np.zeros((actual.shape[0], predicted.shape[1]))
            for i, val in enumerate(actual):
                actual2[i, val] = 1
            actual = actual2

        clip = np.clip(predicted, eps, 1 - eps)
        rows = actual.shape[0]
        vsota = np.sum(actual * np.log(clip))
        return -1.0 / rows * vsota
