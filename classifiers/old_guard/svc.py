from sklearn.calibration import CalibratedClassifierCV
from sklearn.svm import LinearSVC

from classifiers.old_guard.old_gaurd import OldGuard


class SVCTFID(OldGuard):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self._model = CalibratedClassifierCV(LinearSVC(loss='squared_hinge', penalty="l1", dual=False, tol=1e-3))
