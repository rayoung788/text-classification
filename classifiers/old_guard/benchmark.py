from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn import metrics
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.naive_bayes import MultinomialNB
import matplotlib.pyplot as plt

from classifiers.classifier import Classifier
from utils.profile import profile


class Benchmark(Classifier):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self.vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english')
        self.results = []

    @profile
    def transform(self):
        self.split()
        self.x_train = self.vectorizer.fit_transform(self.x_train)
        self.x_test = self.vectorizer.transform(self.x_test)

    def train_model(self, name, clf):
        self._model = clf
        clf_desc = name
        print("Starting " + clf_desc + ":")
        self.train()
        predictions = self.predict(self.x_test)
        score = metrics.accuracy_score(self.y_test, predictions)
        print("accuracy:   %0.3f" % score)
        print("classification report:")
        print(metrics.classification_report(self.y_test, predictions, target_names=self.classes))
        print("confusion matrix:")
        print(metrics.confusion_matrix(self.y_test, predictions))
        self.results.append((clf_desc, score))
        return predictions

    @profile
    def plot(self):
        indices = np.arange(len(self.results))
        results = [[x[i] for x in self.results] for i in range(2)]
        clf_names, score = results
        plt.figure(figsize=(12, 8))
        plt.title("Score")
        plt.barh(indices, score, .2, label="score", color='navy')
        plt.yticks(())
        plt.legend(loc='best')
        plt.subplots_adjust(left=.25)
        plt.subplots_adjust(top=.95)
        plt.subplots_adjust(bottom=.05)
        for i, c in zip(indices, clf_names):
            plt.text(-.3, i, c)
        plt.show()

    @profile
    def benchmark(self):
        classifiers = [
            ("Ridge Classifier", RidgeClassifier(tol=1e-2)),
            #  ("RF", ExtraTreesClassifier(n_estimators=100, n_jobs=-1)),
            ("Passive-Aggressive", PassiveAggressiveClassifier(n_iter=50)),
            ("Linear SVC: l1", LinearSVC(loss='squared_hinge', penalty="l1", dual=False, tol=1e-3)),
            ("Linear SVC: l2", LinearSVC(loss='squared_hinge', penalty="l2", dual=False, tol=1e-3)),
            ("MultinomialNB", MultinomialNB(alpha=.01))
        ]
        for classifier in classifiers:
            print('#' * 80)
            self.train_model(*classifier)
            print('#' * 80)
        self.plot()
