from sklearn import metrics, pipeline

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB

from classifiers.old_guard.old_gaurd import OldGuard


class NaiveBayesTFID(OldGuard):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self._model = MultinomialNB(alpha=0.1)


class NaiveBayesCounts(OldGuard):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self._model = MultinomialNB()
        self.vectorizer = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(1, 3),
                                          stop_words='english')


class NaiveBayesGS(NaiveBayesTFID):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self.param_grid = {'nb__alpha': [0.001, 0.01, 0.1, 1, 10, 100]}
        self.scoring = metrics.make_scorer(self.multiclass_logloss, greater_is_better=False, needs_proba=True)

        clf = pipeline.Pipeline([('nb', self._model)])
        self._model = GridSearchCV(estimator=clf, param_grid=self.param_grid, scoring=self.scoring,
                                   verbose=10, n_jobs=-1, iid=True, refit=True, cv=2)

    def learn(self):
        self.train()
        print("Best score: %0.3f" % self._model.best_score_)
        print("Best parameters set:")
        best_parameters = self._model.best_estimator_.get_params()
        for param_name in sorted(self.param_grid.keys()):
            print("\t%s: %r" % (param_name, best_parameters[param_name]))