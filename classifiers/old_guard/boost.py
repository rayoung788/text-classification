import xgboost as xgb
from sklearn import decomposition

from classifiers.old_guard.old_gaurd import OldGuard
from utils.profile import profile


class XGBoost(OldGuard):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self._model = xgb.XGBClassifier(max_depth=7, n_estimators=200, colsample_bytree=0.8,
                                        subsample=0.8, nthread=10, learning_rate=0.1)

    @profile
    def train(self):
        self._model.fit(self.x_train.tocsc(), self.y_train)

    @profile
    def predict(self, data):
        return self._model.predict_proba(data.tocsc())

    def learn(self):
        self.train()
        predictions = self.predict(self.x_test)
        print("logloss (impl):\t\t {:0.3f}".format(self.multiclass_logloss(self.y_test, predictions)))


class XGBoostSVD(OldGuard):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self.svd = decomposition.TruncatedSVD(n_components=120)
        self._model = xgb.XGBClassifier(max_depth=7, n_estimators=200, colsample_bytree=0.8,
                                        subsample=0.8, nthread=10, learning_rate=0.1)

    @profile
    def transform(self):
        self.split()
        self.vectorizer.fit(self.x_train + self.x_test)
        self.x_train = self.vectorizer.transform(self.x_train)
        self.x_test = self.vectorizer.transform(self.x_test)
        self.svd.fit(self.x_train)
        self.x_train = self.svd.transform(self.x_train)
        self.x_test = self.svd.transform(self.x_test)
