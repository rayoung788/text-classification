from sklearn import metrics

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

from classifiers.classifier import Classifier
from utils.profile import profile


class OldGuard(Classifier):
    def __init__(self, training_data, classes, sub_dir):
        super().__init__(training_data, classes, sub_dir)
        self.vectorizer = TfidfVectorizer(min_df=3, max_features=None,
                                          strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
                                          ngram_range=(1, 3), use_idf=1, smooth_idf=1, sublinear_tf=1,
                                          stop_words='english')
        self.valid_split = 0.1

    def split(self):
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
            self.x, self.y, stratify=self.y, random_state=42, test_size=self.valid_split, shuffle=True)

    @profile
    def predict(self, data):
        return self._model.predict_proba(data)

    @profile
    def transform(self):
        self.split()
        self.vectorizer.fit(self.x_train + self.x_test)
        self.x_train = self.vectorizer.transform(self.x_train)
        self.x_test = self.vectorizer.transform(self.x_test)

    def learn(self):
        self.train()
        predictions = self.predict(self.x_test)
        flat = self._model.predict(self.x_test)
        print("accuracy:\t\t\t {:0.3f}".format(metrics.accuracy_score(self.y_test, flat)))
        print("logloss (impl):\t\t {:0.3f}".format(self.multiclass_logloss(self.y_test, predictions)))
        print("logloss (built-in):\t {:0.3f}".format(metrics.log_loss(self.y_test, predictions)))

