import ujson
from TwitterAPI import TwitterAPI
from flask import Flask, request, render_template
from whitenoise import WhiteNoise
from classifiers.deep_learning.cnn import SmallCNNClassifier
from env import credentials
from praw import Reddit

app = Flask(__name__)

twitter_api = TwitterAPI(*credentials['twitter'])


def twitter(term):
    r = twitter_api.request('search/tweets', {'q': term, 'count': 100, 'lang': 'en'})
    return [item['text'] if 'text' in item else item for item in r]


# reddit_api = Reddit(**credentials['reddit'])
#
#
# def reddit(term):
#     submissons = reddit_api.subreddit(term).hot(limit=25)
#     return submissons


@app.route("/")
def hello():
    return render_template('index.html')


@app.route("/twitter", methods=['GET'])
def twitter_sentiment():
    term = request.args.get('term')
    tweets = twitter(term)
    return ujson.dumps({
        "raw": tweets,
        "sentiment": [pair[1] for pair in app.config["model"].predict(tweets).tolist()]
    })


def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    if request.method == 'OPTIONS':
        response.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT'
        headers = request.headers.get('Access-Control-Request-Headers')
        if headers:
            response.headers['Access-Control-Allow-Headers'] = headers
    return response


if __name__ == "__main__":
    app.after_request(add_cors_headers)
    app.wsgi_app = WhiteNoise(app.wsgi_app, root='static/')
    app.config.update({"model": SmallCNNClassifier([['', 0]], ["negative", "positive"], "twitter")})
    app.run(host="192.168.1.74", port=5555)
