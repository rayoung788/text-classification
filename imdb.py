import codecs
from os import listdir

import re

from pickle import dump, load

from os.path import exists
from tqdm import tqdm

from classifiers.deep_learning.cnn import EmbeddedCNNClassifier, CNNClassifier, SmallCNNClassifier
from classifiers.old_guard.benchmark import Benchmark

path = "./data/imdb/all"
imdb_path = path + "/imdb.p"


def import_imdb():
    pos_dir = path + "/pos/"
    neg_dir = path + "/neg/"
    positives = [pos_dir + f for f in listdir(pos_dir)]
    negatives = [neg_dir + f for f in listdir(neg_dir)]
    docs = [codecs.open(f, 'r', "utf-8").read() for i, f in enumerate(tqdm(positives + negatives))]
    data = zip(docs, [0] * len(positives) + [1] * len(negatives))
    classes = ["positive", "negative"]
    dump((data, classes), open(imdb_path, "wb"))
    return data, classes


def collect():
    return load(open(imdb_path, "rb")) if exists(imdb_path) else import_imdb()


def old_guard():
    data, classes = collect()
    cls = Benchmark(data, classes, "imdb")
    cls.benchmark()


def cnn():
    data, classes = collect()
    cls = SmallCNNClassifier(data, classes, "imdb")
    cls.train()


if __name__ == '__main__':
    old_guard()

