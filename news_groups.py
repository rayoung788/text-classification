from sklearn.datasets import fetch_20newsgroups

from classifiers.deep_learning.cnn import EmbeddedCNNClassifier, SmallCNNClassifier, CNNClassifier
# from classifiers.deep_learning.fasttext import Fasttext
from classifiers.old_guard.benchmark import Benchmark

remove = ('headers', 'footers', 'quotes')


def collect():
    training = fetch_20newsgroups(subset='all', shuffle=True, random_state=42, remove=remove)
    return zip(training.data, training.target), training.target_names


def old_guard():
    data, classes = collect()
    cls = Benchmark(data, classes, "20newsgroups")
    cls.benchmark()


def cnn():
    data, classes = collect()
    cls = SmallCNNClassifier(data, classes, "20newsgroups")
    cls.train()


if __name__ == '__main__':
    cnn()
